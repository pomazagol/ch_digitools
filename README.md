## Série de séminaires internationaux *Héritages culturels et outils numériques*


L'Université Jean Monnet - Saint-Étienne, sa Faculté des Sciences Humaines et Sociales et le laboratoire Environnement - Ville - Société - UMR 5600 - CNRS ont lancé une série de séminaires internationaux annuels portant sur la thématique de l'usage des outils numériques (géomatiques, 3D, reconsitution sonores...) pour létude et la gestion des héritages culturels.

Les deux premières étitions ont donné une grande place aux collègues ayant pris part au **partenariat startégique ERASMUS+ [MINERVA](https://minerva-erasmus.com/)**. Vous pouvez consulter les programmes et ressources associées des éditions [2022](https://pomazagol.gitpages.huma-num.fr/ch_digitools/seminar_2022.html) et [2023](https://pomazagol.gitpages.huma-num.fr/ch_digitools/seminar_2023.html).

La prochaine édition qui, se tiendra le 20 novembre 2024, met notamment en avant les collègue de l'alliance [T4EU](https://transform4europe.eu/).

**Le programme est disponible [ici](https://pomazagol.gitpages.huma-num.fr/ch_digitools/seminar_2023.html)**.

L'[inscription](https://framaforms.org/inscription-registration-3eme-seminaire-international-heritages-culturels-et-outils-numeriques-a/) est obligatoire ! 

![ Série de séminaires internationaux *Héritages culturels et outils numériques*](public/illustrations/seminar.jpg " Série de séminaires internationaux *Héritages culturels et outils numériques*")
